package com.test.spring.utils;

import java.text.SimpleDateFormat;
import java.util.Date;



public class StringUtil {
	
	public static String testUtil() {
		return "util";
	}
	
	public static String dateTimeToString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constant.defaultFormatTimeStamp);
		return dateFormat.format(date);
	}
	
	public static String dateToString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constant.defaultFormatDt);
		return dateFormat.format(date);
	}
}
