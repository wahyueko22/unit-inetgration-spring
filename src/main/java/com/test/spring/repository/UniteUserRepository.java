package com.test.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.test.spring.entity.UnitUser;

@Repository
public interface UniteUserRepository extends JpaRepository<UnitUser, Long> {
	
	@Query(value = "select * from unit_user where email=:email ", nativeQuery = true)
	public UnitUser findUserByEmail(@Param("email") String email);
}
