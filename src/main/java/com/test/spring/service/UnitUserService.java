package com.test.spring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.test.spring.entity.UnitUser;
import com.test.spring.object.response.UnitUserResponse;
import com.test.spring.repository.UniteUserRepository;

@Service
public class UnitUserService {
	private Logger logger = LoggerFactory.getLogger(UnitUserService.class);
	
	private UniteUserRepository uniteUserRepository;
		
	public UnitUserService(UniteUserRepository uniteUserRepository) {
		this.uniteUserRepository = uniteUserRepository;
	}
	
	public String getUserTest() {
		logger.info("okkk");
		System.out.println("okkkkk");
		return "ok";
	}
	
	public UnitUserResponse getUserByEmail(String email) {
		UnitUser unitUser= uniteUserRepository.findUserByEmail(email);
		UnitUserResponse unitUserRes = new UnitUserResponse();
		BeanUtils.copyProperties(unitUser, unitUserRes);
		return unitUserRes;
	}
	
	
}
