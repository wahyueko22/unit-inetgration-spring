package com.test.spring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.spring.object.response.BaseResponseDTO;
import com.test.spring.object.response.UnitUserResponse;
import com.test.spring.service.UnitUserService;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UnitUserController {
	
	private UnitUserService unitUserService;
	
	
	
	public UnitUserController(UnitUserService unitUserService) {
		this.unitUserService = unitUserService;
	}
	
	@GetMapping("/test")  
	public ResponseEntity<Object>  getUserTest() {
		unitUserService.getUserTest();
		return new ResponseEntity<Object>("ok", HttpStatus.OK);
	}
	
	public String strUserTest() {
		return unitUserService.getUserTest();
	}
	
	@GetMapping("/by-email/{email}")  
	public ResponseEntity<Object>  getUserByEmail(@PathVariable("email") String email) {
		UnitUserResponse unitUserResponse =unitUserService.getUserByEmail(email);
		BaseResponseDTO baseDto = new BaseResponseDTO("Ok sukses", null ,"000", "sukses", unitUserResponse);
		return new ResponseEntity<Object>(baseDto, HttpStatus.OK);
	}
}
