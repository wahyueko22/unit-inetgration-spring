package com.test.spring.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;

import com.test.spring.UnitTestSpringApplication;

@SpringBootTest(classes = UnitTestSpringApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class ControllerUserIntegrationRestTemplate {
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
		
	@Test
    public void getUserTest() 
	{
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<?> request = new HttpEntity<>(headers);
		ResponseEntity<String> strRes = this.restTemplate.exchange("http://localhost:" + port + "/user/test", HttpMethod.GET,request, String.class);
		assertThat(strRes.getStatusCode().value()).isEqualTo(200);
	}
	
	@Test
    public void getUserByEmail() 
	{
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<?> request = new HttpEntity<>(headers);
		ResponseEntity<String> strRes = this.restTemplate.exchange("http://localhost:" + port + "/user/by-email/ani@gmail.com", HttpMethod.GET,request, String.class);
		System.out.println(strRes.getBody());
		assertThat(strRes.getStatusCode().value()).isEqualTo(200);
	}
}
