package com.test.spring.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import com.test.spring.repository.UniteUserRepository;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class UniteUserRepositoryTest {
	@Autowired
	private UniteUserRepository uniteUserRepository;
	
		
	@Test
	void onlyUniteUserRepositoryIsLoaded() {
		System.out.println("masukk UniteUserRepositoryTest");
		assertThat(uniteUserRepository).isNotNull();
	}
}
