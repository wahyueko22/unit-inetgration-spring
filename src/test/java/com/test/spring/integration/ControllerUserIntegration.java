package com.test.spring.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

//https://dzone.com/articles/integration-testing-what-it-is-and-how-to-do-it-ri
//https://stackoverflow.com/questions/54658563/unit-test-or-integration-test-in-spring-boot
// integration test, is not recommaneded to use, but if not possible to use realtime, 
// it is allowed to use mock
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerUserIntegration {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void getUserTest() throws Exception {
		System.out.println("integration controller");
		this.mockMvc.perform(get("/user/test")).andExpect(status().isOk());
	}
	
	@Test
	public void getUserByEmail() throws Exception {
		this.mockMvc.perform(get("/user/by-email/ani@gmail.com")).andExpect(status().isOk());
	}
	
}
