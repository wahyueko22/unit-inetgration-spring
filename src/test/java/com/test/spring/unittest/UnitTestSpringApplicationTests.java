package com.test.spring.unittest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

import com.test.spring.controller.UnitUserController;

//https://reflectoring.io/spring-boot-test/
//@SpringBootTest
class UnitTestSpringApplicationTests {
	@Autowired
	private UnitUserController controller;
	
	/*
	 * public UnitTestSpringApplicationTests() { this.controller = new
	 * UnitUserController(); }
	 */
	
	//@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
