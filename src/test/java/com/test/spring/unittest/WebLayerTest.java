package com.test.spring.unittest;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;

import com.test.spring.controller.UnitUserController;
import com.test.spring.service.UnitUserService;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

//@WebMvcTest
public class WebLayerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean// hanya di spring
	private UnitUserService service;
	
	//@Test
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/user/test")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello, World")));
	}
	
}
