package com.test.withoutspring.unittest;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Date;

import static org.mockito.Mockito.verify;

import com.test.spring.utils.StringUtil;

public class WithoutStringUtilTest {
	
	@Test
	public void contructorTest() {
		StringUtil util = new StringUtil();
		assertThat(util).isNotNull();
	}
	
	@Test
	public void testUtilTest() {
		assertThat(StringUtil.testUtil()).isEqualTo("util");
	}
	
	@Test
	public void dateTimeToString() {
		String strDt = StringUtil.dateTimeToString(new Date());
		assertThat(strDt).isNotNull();
	}
	
	@Test
	public void dateToString() {
		String strDt = StringUtil.dateToString(new Date());
		assertThat(strDt).isNotNull();
	}
}
