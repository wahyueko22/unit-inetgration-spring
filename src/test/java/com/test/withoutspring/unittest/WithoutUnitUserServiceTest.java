package com.test.withoutspring.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.test.spring.controller.UnitUserController;
import com.test.spring.entity.UnitUser;
import com.test.spring.object.response.UnitUserResponse;
import com.test.spring.repository.UniteUserRepository;
import com.test.spring.service.UnitUserService;

public class WithoutUnitUserServiceTest {
	private UniteUserRepository uniteUserRepository  = Mockito.mock(UniteUserRepository.class);
		
	private UnitUserService unitUserService;
	private final String email = "ani@gmail.com"; 
	private UnitUser unitUser = null;
	private UnitUserResponse unitUserRes = null;

	@BeforeEach
	void setup() {
		this.unitUserService = new UnitUserService(uniteUserRepository);
		unitUser = new UnitUser();
		unitUser.setId(1);
		unitUser.setName("ani");
		unitUser.setEmail("ani@gmail.com");
		unitUser.setAddress("Koja");
		
		unitUserRes = new UnitUserResponse(); 
		unitUserRes.setId(1);
		unitUserRes.setName("ani");
		unitUserRes.setEmail("ani@gmail.com");
		unitUserRes.setAddress("Koja");
		
	}
	
	@Test
	public void getUserTest() {
		String res = unitUserService.getUserTest();
		assertEquals("ok", res);
	}
	
	@Test
	public void getUserByEmailTest() {
		Mockito.when(uniteUserRepository.findUserByEmail(email)).thenReturn(unitUser);
		UnitUserResponse testRes = unitUserService.getUserByEmail(email);
		assertNotNull(testRes);
		assertEquals(testRes.getName(), unitUser.getName());
		assertEquals(testRes.getEmail(), unitUser.getEmail());
		assertEquals(testRes.getAddress(), unitUser.getAddress());
		assertEquals(testRes.getId(), unitUser.getId());
	}
	
	
	
}
