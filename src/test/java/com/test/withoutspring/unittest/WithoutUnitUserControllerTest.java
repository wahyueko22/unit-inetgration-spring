package com.test.withoutspring.unittest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import com.test.spring.controller.UnitUserController;
import com.test.spring.object.response.BaseResponseDTO;
import com.test.spring.object.response.UnitUserResponse;
import com.test.spring.service.UnitUserService;

//https://github.com/ragcrix/StudentInformationSystem
//https://github.com/thombergs/code-examples/tree/master/spring-boot/spring-boot-testing

public class WithoutUnitUserControllerTest {
	//@MockBean   //annotation for spring only
	//private UnitUserService service;
	
	private final UnitUserService service = Mockito.mock(UnitUserService.class);
	
	private final UnitUserController nitUserController = new UnitUserController(service);
	private UnitUserResponse unitUserRes = null;
	private BaseResponseDTO baseDto = null;

	@BeforeAll
	public static void init() {
		System.out.println("Before All init() method called");
	}
	 
	@BeforeEach
	void setup() {
		System.out.println("TESTTTTTTTT");
			
		unitUserRes = new UnitUserResponse(); 
		unitUserRes.setId(1);
		unitUserRes.setName("ani");
		unitUserRes.setEmail("ani@gmail.com");
		unitUserRes.setAddress("Koja");
		
	}

	@Test
	public void strUserTest() {
		when(service.getUserTest()).thenReturn("ok");
		assertThat(nitUserController).isNotNull();
		assertThat(nitUserController.strUserTest()).isEqualTo("ok");
	}
	
	@Test
	public void getUserTest() {
		when(service.getUserTest()).thenReturn("ok");
		ResponseEntity<Object> res = nitUserController.getUserTest();
		verify(service).getUserTest();
		assertThat(res).isNotNull();
		assertThat(res.getBody()).isEqualTo("ok");
	}
	
	@Test
	public void getUserByEmail() {
		String email ="ani@gmail.com";
		when(service.getUserByEmail(email)).thenReturn(unitUserRes);
		ResponseEntity<Object> res = nitUserController.getUserByEmail(email);
		assertThat(res).isNotNull();
	}

	
	
}
